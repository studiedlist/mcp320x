# `mcp320x`

> Platform agnostic driver for the MCP320X ADC's built using
> the *embedded-hal*.

Fork of https://github.com/pcein/adc-mcp3008 

## Documentation

- [API Docs](https://docs.rs/mcp320x)

- [Example programs using this library](https://gitlab.com/studiedlist/mcp320x-examples)
- [Example programs using similar mcp3008 library](https://github.com/pcein/mcp3008-examples)

- [Tutorial on writing embedded-hal based platform agnostic drivers](http://pramode.in/2018/02/24/an-introduction-to-writing-embedded-hal-based-drivers-in-rust/)


## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the
work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

